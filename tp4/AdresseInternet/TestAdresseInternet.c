#include "AdresseInternetType.h"
#include <stdio.h>

int main (void) {
    char input[BUFSIZ];
    printf("Adresse : ");
    scanf("%s", input);
    AdresseInternet *addr = AdresseInternet_new(input, 80);
    char buf[BUFSIZ];
    AdresseInternet_getIP(addr, buf, BUFSIZ);
    printf("Adresse IP: %s\n", buf);
    AdresseInternet_free(addr);
    addr = AdresseInternet_any(8080);
    printf("Test de any ! \n");
    printf("Nom: %s, Port: %s\n", addr->nom, addr->service);
    AdresseInternet_free(addr);
    addr = AdresseInternet_loopback(8080);
    printf("Test de loopback ! \n");
    printf("Nom: %s, Port: %s\n", addr->nom, addr->service);
    AdresseInternet_free(addr);
    return 0;
}
