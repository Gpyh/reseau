#ifndef _INET_ADDRESS_H
#define _INET_ADDRESS_H

#include <stdint.h>

#define INET_ADDRESS_DNS_MAX_SIZE 256
#define INET_ADDRESS_SERVICE_MAX_SIZE 20

typedef struct {
  struct sockaddr_storage sockaddr;
  char name[INET_ADDRESS_DNS_MAX_SIZE];
  char service[INET_ADDRESS_SERVICE_MAX_SIZE];
} inet_address;

inet_address *inet_address_new(const char* address, uint16_t port);

inet_address *inet_address_any(uint16_t port);

inet_address *inet_address_loopback(uint16_t port);

#endif
