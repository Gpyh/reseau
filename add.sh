#!/bin/bash

FILE=cmds.adoc
CMD=$(history 2 | head -1 | cut -c 8-)
echo $CMD
echo -e "==Commande\n\n----\n$CMD\n----\n" >> $FILE
if [[ $1 == "--no-output" ]]; then
	echo "no output"
else
	echo "Output:\n\n----" >> $FILE
	bash -c "$CMD >> $FILE"
	echo -e "----\n" >> $FILE
fi

