enum GUESS_RESULT {
  SAME = 0,
  TOOBIG = 1,
  ERROR,
  TOOSMALL = -1
};
