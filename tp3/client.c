#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "adresse_serveur_unix.h"
#include "guess_protocol.h"

int main(int argc, char* argv[]) {
  int s;

  if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    return EXIT_FAILURE;
  }

  struct sockaddr_un addr = {
    .sun_family = AF_UNIX,           // sun_family
    .sun_path   = ADRESSE_SERVEUR    // sun_path
  };

  if (connect(s, (struct sockaddr*) &addr, sizeof(addr)) == -1) {
    perror("connect");
    return EXIT_FAILURE;
  }

  printf("You are playing \"Guess!\".\nTry to guess a number between 0 and 1000.");
  int guess;
  enum GUESS_RESULT result = ERROR;
  do {
    printf("\nYour guess: ");
    scanf("%d", &guess);
    if (write(s, &guess, sizeof (int)) == -1) {
      perror("write");
      return EXIT_FAILURE;
    }
    printf("%d, %d\n",result, guess);
  } while(result != SAME);
  
  if (close(s) == -1) {
    perror("close");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
