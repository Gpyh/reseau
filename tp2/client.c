/**
 * Un client UDP qui se contente d'envoyer un bon jour par datagramme.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#define ADDR_SERVER "sunecho"

int main(void) {
  int s;

  if ((s = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
    perror("socket");
    return EXIT_FAILURE;
  }

  struct sockaddr_un addr;
  memset(&addr, '\0', sizeof (struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, ADDR_SERVER, (sizeof (addr.sun_path)) - 1);

  if (sendto(s, "bonjour", 8, 0, (struct sockaddr*) &addr, sizeof(struct sockaddr_un)) == -1) {
    perror("sendto");
    return EXIT_FAILURE;
  }

  if (close(s) == -1) {
    perror("close");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
