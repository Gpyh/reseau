/**
 * Un mini serveur UDP qui se contente d'afficher le premier message qu'il reçoit.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#define BUF_SIZE 512
#define ADDR_SERVER "sunecho"

int main(void) {
  int s;

  if ((s = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
    perror("socket");
    return EXIT_FAILURE;
  }

  struct sockaddr_un addr;
  memset(&addr, '\0', sizeof (struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, ADDR_SERVER, (sizeof (addr.sun_path)) - 1);

  if (bind(s, (struct sockaddr*) &addr, sizeof(struct sockaddr_un)) == -1) {
    perror("bind");
    return EXIT_FAILURE;
  }

  char buf[BUF_SIZE];
  buf[BUF_SIZE - 1] = 0;
  struct sockaddr_un addr_from;
  socklen_t addr_len = sizeof(addr_from);
  if (recvfrom(s, buf, sizeof(buf) - 1, 0, (struct sockaddr*) &addr_from, &addr_len) == -1) {
    perror("recvfrom");
    return EXIT_FAILURE;
  }

  if (addr_len == 0) {
    strcpy(addr_from.sun_path, "inconnu");
  } 

  printf("Message reçu de %s:%s\n", addr_from.sun_path, buf);

  if (unlink(ADDR_SERVER) == -1) {
    perror("unlink");
    exit(EXIT_FAILURE);
  }

  if (close(s) == -1) {
    perror("close");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
